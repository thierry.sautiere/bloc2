## Le voyageur

- Une version itérative réalisée.

    ```python
        def voyageur_glouton(ville, liste_villes, matrice_distance) :
            
            indice = indice_ville(ville, liste_villes)
            tour_glouton = [liste_villes[indice]]
            ville= liste_villes[indice][0]  
            
            while len(tour_glouton) < len(liste_villes) :
                deselectionner_une_ville(ville, liste_villes, matrice_distance)
                ville = liste_villes[ville_la_plus_proche(ville, liste_villes, matrice_distance)][0]
                tour_glouton.append(liste_villes[indice_ville(ville, liste_villes)])
                     
            return tour_glouton
    ```


- Une version récursive réalisée.

   ```python
       def voyageur_glouton_recu(ville, liste_villes, matrice_distance, tour_glouton) :
    
            if len(tour_glouton) == len(liste_villes) :
                return tour_glouton
            else :
                deselectionner_une_ville(ville, liste_villes, matrice_distance)
                ville = liste_villes[ville_la_plus_proche(ville, liste_villes, matrice_distance)][0]
                tour_glouton.append(liste_villes[indice_ville(ville, liste_villes)])
                voyageur_glouton_recu(ville, liste_villes, matrice_distance, tour_glouton)
            
            return tour_glouton
    ```
    ![voyageur](images/voyageur.png)
    
    
