import TSP_biblio as tsp
import math

'''
l'heuristique de choix de la solution optimale locale : Distance la plus courte pour aller à la ville suivante non encore visitée.
'''

def matrice_distances_villes(f) :
    
    '''
    La fonction matrice_distances_villes donne la matrice des distances entre deux villes du fichier texte f.
    
    @parametres
        f est une chaine de caractères <str> du nom du fichier texte qui contient sur une ligne le nom de la ville et ses coordonnées :
                    Annecy  6,082499981 45,8782196 etc…
                    
    @return
        la matrice des distances entre deux villes du fichier texte de nom f
        <list>
        
     @exemple
         >>> matrice_distances_villes('exemple.txt')[0][0]
         0.0
         >>> matrice_distances_villes('exemple.txt')[-1][-1]
         0.0
            
    '''
    
    tour = tsp.get_tour_fichier(f)
    
    return [[tsp.distance (tour, i,j) for i in range(len(tour))] for j in range(len(tour))]

def liste(f) :    
   
    return tsp.get_tour_fichier(f)

def  ville_la_plus_proche(ville, liste_villes, matrice_distance) :
    
    distance_chaque_ville = [math.inf if not indice_ville(ville, liste_villes)-i else i for i in matrice_distance[indice_ville(ville, liste_villes)]]
       
    return distance_chaque_ville.index(min(distance_chaque_ville))

def indice_ville(ville, liste_villes) :
    
    villes_sans_coordonnees = [j[0] for j in liste_villes]
    return villes_sans_coordonnees.index(ville)

def deselectionner_une_ville(ville, liste_villes, matrice_distance) :
    
    indice = indice_ville(ville, liste_villes)
    
    for i in range(len(matrice_distance)) :
        matrice_distance[i][indice] = math.inf

def voyageur_glouton(ville, liste_villes, matrice_distance) :
    
    indice = indice_ville(ville, liste_villes)
    tour_glouton = [liste_villes[indice]]
    ville= liste_villes[indice][0]  
    
    while len(tour_glouton) < len(liste_villes) :
        deselectionner_une_ville(ville, liste_villes, matrice_distance)
        ville = liste_villes[ville_la_plus_proche(ville, liste_villes, matrice_distance)][0]
        tour_glouton.append(liste_villes[indice_ville(ville, liste_villes)])
             
    return tour_glouton

def voyageur_glouton_recu(ville, liste_villes, matrice_distance, tour_glouton) :
    
    '''
    Fonction récursive terminale
    '''
    
    if len(tour_glouton) == len(liste_villes) :
        return tour_glouton
    else :
        deselectionner_une_ville(ville, liste_villes, matrice_distance)
        ville = liste_villes[ville_la_plus_proche(ville, liste_villes, matrice_distance)][0]
        tour_glouton.append(liste_villes[indice_ville(ville, liste_villes)])
        voyageur_glouton_recu(ville, liste_villes, matrice_distance, tour_glouton)
        return tour_glouton

if __name__ == '__main__':
    import doctest
    doctest.testmod()
   # tsp.trace (voyageur_glouton_recu('Annecy', liste('exemple.txt'), matrice_distances_villes('exemple.txt'),[]))
    tsp.trace (voyageur_glouton('Annecy', liste('exemple.txt'), matrice_distances_villes('exemple.txt')))